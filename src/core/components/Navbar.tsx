import React from 'react';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { selectProductsByCat, selectTotalProducts } from '../../features/catalog/store/products.selectors';
import { usePrefetch } from '../../features/users/store/users.api';

const activeStyle: React.CSSProperties = { color: 'orange'};

export const Navbar = () => {
  const total = useSelector(selectTotalProducts)
  const prefetchUsers = usePrefetch('getUsersSmall')

  return (
    <nav className="navbar navbar-expand navbar-light bg-light">
      <div className="navbar-brand">
        <NavLink
          style={({ isActive }) => isActive ? activeStyle : {}}
          className="nav-link"
          to="/">
          REDUX {total}
        </NavLink>
      </div>

      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item">
            <NavLink style={(obj) => obj.isActive ? activeStyle  : {} }
                     className="nav-link"
                     to="/settings">
              <small>settings</small>
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/counter">
              <small>counter</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/users"
              onMouseOver={() => prefetchUsers()}
            >
              <small>users</small>
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink
              style={({ isActive }) =>
                isActive ? activeStyle : {}
              }
              className="nav-link"
              to="/catalog">
              <small>catalog</small>
            </NavLink>
          </li>

        </ul>
      </div>
    </nav>
  )
}

import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { AppThunk } from '../../../App';
import { setHttpStatus } from '../../../core/store/http-status.store';
import { Product } from '../model/product';
import {
  addProductSuccess,
  deleteProductSuccess,
  getAllProducts,
  getProductsSuccess, setError,
  toggleProductVisibilitySuccess
} from './products.store';


export const getProducts = createAsyncThunk<Product[], void>(
  'products/get',
  async (payload, { dispatch, rejectWithValue}) => {
    try {
      const res = await axios.get<Product[]>('http://localhost:3001/products')
      // dispatch(getProductsSuccess(res.data))
      return res.data;
    } catch(err) {
      return rejectWithValue('erroraccio!')
    }
  }
)


export const getProductsClassic = (): AppThunk => async dispatch => {
  // dispatch(getAllProducts())
  try {
    const res = await axios.get<Product[]>('http://localhost:3001/products')
    dispatch(getProductsSuccess(res.data))
    dispatch(setHttpStatus({ status: 'success', actionType: 'get'}))

  } catch (e) {
    dispatch(setError());
    dispatch(setHttpStatus({ status: 'error', actionType: getProductsSuccess.type}))
  }
}


export const deleteProduct = (id: number): AppThunk => async dispatch => {
  try {
    await axios.delete<void>(`http://localhost:3001/products/${id}`);
    dispatch(deleteProductSuccess(id))
  } catch (err) {
    // dispatch error
  }
};

/*
export const addProduct = (
  product: Partial<Product>
): AppThunk => async dispatch => {

  try {
    const newProduct = await axios.post<Product>('http://localhost:3001/products', {
        ...product,
        visibility: false
      }
    );
    dispatch(addProductSuccess(newProduct.data))
  } catch (err) {
    dispatch(setError())
    dispatch(setHttpStatus({ status: 'error', actionType: 'add'}))
    // dispatch error
  }
};
 */
export const addProduct = createAsyncThunk<Product, Partial<Product>>(
  'products/add',
  async (payload, {  dispatch, rejectWithValue })  => {
    try {
      const newProduct = await axios.post<Product>(
        'http://localhost:3001/products',
        { ...payload, visibility: false }
      );
      dispatch(addProductSuccess(newProduct.data))
      return newProduct.data;
    } catch (err) {
      dispatch(setHttpStatus({ status: 'error', actionType: 'addProduct' }))
      return rejectWithValue('errore!')
    }
  }
)


export const toggleProduct = (product: Product): AppThunk => async dispatch => {
  try {
    const updatedProduct: Product = {
      ...product,
      visibility: !product.visibility
    };
    const response = await axios.patch<Product>(`http://localhost:3001/products/${product.id}`, updatedProduct);
    dispatch(toggleProductVisibilitySuccess(response.data))
  } catch (err) {
    // dispatch error
  }
};

import { RootState } from '../../../App';
import { Product } from '../model/product';

export const selectProductsError = (state: RootState) => state.catalog.list.error;

export const selectProducts = (state: RootState) => state.catalog.list.items
export const selectTotalProducts = (state: RootState) => state.catalog.list.items.length
export const selectTotalCostProducts = (state: RootState) => {
  return state.catalog.list.items.reduce((acc: number, item: Product) => {
    return acc + item.price
  }, 0)
}


export const selectProductsByCat =  (state: RootState) => {
  const catId = state.catalog.categories.activeCategoryId
  return state.catalog.list.items.filter(item => {
    return catId === -1 ? true : item.categoryId === catId
  })
}

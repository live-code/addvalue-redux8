// features/catalog/store/categories.store.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export const categoriesStore = createSlice({
  name: 'categories',
  initialState: {
    list:  [
      { id: -1, name: 'All'},
      { id: 1, name: 'Latticini'},
      { id: 2, name: 'Dolci'},
    ],
    activeCategoryId: -1,
  },
  reducers: {
    changeActiveCategoryId(state, action: PayloadAction<number>) {
      state.activeCategoryId = action.payload;
      // return { ...state, activeCategoryId: action.payload}
    }
  }
})

export const { changeActiveCategoryId } = categoriesStore.actions

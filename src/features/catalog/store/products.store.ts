import { createAction, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { increment } from '../../counter/store/counter/counter.actions';
import { Product } from '../model/product';
import { getProducts } from './products.actions';

export const getAllProducts = createAction<void>('products/get products')


export const productsStore = createSlice({
  name: 'products',
  initialState: {
    items: [] as Product[],
    error: false
  },
  reducers: {
    getProductsSuccess(state, action: PayloadAction<Product[]>) {
      state.error = false;
      state.items = action.payload
    },
    addProductSuccess(state, action: PayloadAction<Product>) {
      state.error = false;
      state.items.push(action.payload)
    },
    deleteProductSuccess(state, action: PayloadAction<number>) {
      state.error = false;
      const index = state.items.findIndex(p => p.id === action.payload)
      state.items.splice(index, 1)
    },
    toggleProductVisibilitySuccess(state, action: PayloadAction<Product>) {
      state.error = false;
      const product = state.items.find(p => p.id === action.payload.id);
      if (product) {
        product.visibility = action.payload.visibility;
      }
    },
    setError(state) {
      state.error = true;
    }
  },
  extraReducers: builder =>
    builder
      .addCase(getProducts.fulfilled, (state, action) => {
        state.error = false;
        state.items = action.payload
      })
      .addCase(getProducts.rejected, (state, action) => {
        state.error = true
      })
      .addCase(increment, (state) => {
        state.items = [];
      })

})
export const {
  addProductSuccess,
  toggleProductVisibilitySuccess,
  deleteProductSuccess,
  getProductsSuccess,
  setError
} = productsStore.actions;

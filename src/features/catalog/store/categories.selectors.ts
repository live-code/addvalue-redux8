import { RootState } from '../../../App';

export const selectCategories = ((state: RootState) => state.catalog.categories.list)
export const selectActiveCategoryId = ((state: RootState) => state.catalog.categories.activeCategoryId)

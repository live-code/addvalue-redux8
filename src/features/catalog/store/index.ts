import { combineReducers } from '@reduxjs/toolkit';
import { categoriesStore } from './categories.store';
import { productsStore } from './products.store';

export const catalogReducers = combineReducers({
  list: productsStore.reducer,
  categories: categoriesStore.reducer,
})

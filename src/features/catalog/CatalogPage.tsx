import { unwrapResult } from '@reduxjs/toolkit';
import classNames from 'classnames';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useAppDispatch } from '../../App';
import { selectHttpStatus } from '../../core/store/http-status.store';
import { ProductsForms } from './components/ProductsForm';
import { Product } from './model/product';
import { selectActiveCategoryId, selectCategories } from './store/categories.selectors';
import { changeActiveCategoryId } from './store/categories.store';
import { addProduct, deleteProduct, getProducts, toggleProduct } from './store/products.actions';
import {
  selectProducts,
  selectProductsByCat, selectProductsError,
  selectTotalCostProducts,
} from './store/products.selectors';
import { addProductSuccess, getProductsSuccess } from './store/products.store';

export const CatalogPage = () => {
  const dispatch = useAppDispatch()
  const total = useSelector(selectTotalCostProducts)
  const activeCategoryID = useSelector(selectActiveCategoryId)
  const categories = useSelector(selectCategories)
  const products = useSelector(selectProductsByCat)
  const error = useSelector(selectProductsError)
  const httpStatus = useSelector(selectHttpStatus)

  useEffect(() => {
    dispatch(getProducts())
      .then(unwrapResult)
      .then((res) => console.log('success', res))
      .catch(err => console.log('errore!!', err))
  }, [dispatch])

  function addTodoHandler(formData: Partial<Product>) {
      dispatch(addProduct(formData))
        .then(res => {
          console.log('success', res)
        })

  }

  return <div>
    <h1>Catalog</h1>

    {(httpStatus.status === 'error' && httpStatus.actionType === 'add') &&
      <div className="alert alert-danger">Add operation failed!</div>}


    <ProductsForms onSubmit={addTodoHandler} />

    {/*FILTER LIST*/}
    {
      // NEW: FILTER CATEGORY
      categories.map(c => {
        return (
          <button
            key={c.id}
            className={classNames(
              'btn',
              { 'btn-primary': c.id === activeCategoryID },
              { 'btn-outline-primary': c.id !== activeCategoryID }
            )}
            onClick={() => dispatch(changeActiveCategoryId(c.id))}
          >
            {c.name}
          </button>
        )
      })
    }
    {/*PRODUCT LIST */}
    {
      products.map(p => {
        return <li key={p.id}>
          {p.title} {p.price}

          <i
            className="fa fa-trash"
            onClick={() => dispatch(deleteProduct(p.id))}
          ></i>

           <span onClick={() => dispatch(toggleProduct(p))}>
              {
                p.visibility ?
                  <i className="fa fa-eye" /> :
                  <i className="fa fa-eye-slash" />
              }
            </span>
        </li>
      })
    }
    <hr/>
    TOTAL: € {total}
  </div>
};


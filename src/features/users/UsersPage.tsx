import { useDeleteUserMutation, useGetUserByIdQuery, useGetUsersQuery, useGetUsersSmallQuery } from './store/users.api';

export const UsersPage = () => {
  const { data,  error, isLoading } = useGetUsersQuery()

  const [
    deleteUser,
    { isLoading: isLoadingDelete, error: errorDelete}
  ] = useDeleteUserMutation();

  return <div>

    <div>isLoading: {JSON.stringify(isLoading)}</div>
    <div>error: {JSON.stringify(error)}</div>
    <h1>data</h1>
    {
      data?.map((u) =>
        <li key={u.id}>
          {u.title}
          <i className="fa fa-trash"
             onClick={() => deleteUser(u.id)}></i>
          {isLoadingDelete && <span>loading...</span>}
        </li>)
    }
    <hr/>

  </div>
};

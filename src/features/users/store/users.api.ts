import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { User } from '../model/user';

type SmallUser = { id: number, title: string};

export const usersApi = createApi({
  reducerPath: 'users',
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:3001/'}),
  tagTypes: ['Users', 'Posts', '....'],
  endpoints: (builder) => ({
    getUsers: builder.query<User[], void>({
      query: () => '/users'
    }),
    getUsersSmall: builder.query<SmallUser[], void>({
      query: () => 'users',
      transformResponse: (res: User[]) => {
        return res.map(u => {
          return { id: u.id, title: '...' + u.name}
        })
      },
      keepUnusedDataFor: 2,
      providesTags: ['Users']
    }),
    getUserById: builder.query<User, number>({
      query: (id) => `/users/${id}`
    }),
    search: builder.query<User[], string>({
      query: (text) => `/users?q=${text}`
    }),
    deleteUser: builder.mutation({
      query: (id) => ({
        url: `users/${id}`,
        method: 'DELETE'
      }),
      invalidatesTags: ['Users']
    })
  })
})

export const {
  useGetUsersQuery,
  useGetUserByIdQuery,
  useGetUsersSmallQuery,
  useDeleteUserMutation,
  useSearchQuery,
  usePrefetch
} = usersApi

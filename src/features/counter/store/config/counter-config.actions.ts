import { createAction } from '@reduxjs/toolkit';
import { CounterConfig } from '../../model/counter-config';

export const setConfig = createAction<Partial<CounterConfig>>('set counter config')


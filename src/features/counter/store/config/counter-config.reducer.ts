import { createReducer, PayloadAction } from '@reduxjs/toolkit';
import { CounterConfig } from '../../model/counter-config';
import { setConfig } from './counter-config.actions';

const INITIAL_STATE: CounterConfig = {
  itemsPerPallet: 10, material: 'wood'
};
/*
export const counterConfigReducerOLDSTYLE = createReducer<CounterConfig>(INITIAL_STATE, {
  [setConfig.type]: (state, action: PayloadAction<Partial<CounterConfig>>) => {
    return {...state, ...action.payload }
  }
})*/


export const counterConfigReducer = createReducer<CounterConfig>(INITIAL_STATE, builder =>
  builder
    .addCase(setConfig, (state, action) => ( {...state, ...action.payload }) )
)


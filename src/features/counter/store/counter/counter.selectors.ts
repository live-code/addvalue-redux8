import { RootState } from '../../../../App';

export const selectItemsPerPallet = (state: RootState) => state.counter.config.itemsPerPallet
export const selectMaterial = (state: RootState) => state.counter.config.material


export const selectCounter = (state: RootState) => state.counter.value
export const selectTotalPallet = (state: RootState) =>
  Math.ceil(state.counter.value / state.counter.config.itemsPerPallet)

import { useState } from 'react';
import { Root } from 'react-dom/client';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../App';
import { setConfig } from './store/config/counter-config.actions';
import { decrement, increment } from './store/counter/counter.actions';
import { selectCounter, selectItemsPerPallet, selectMaterial, selectTotalPallet } from './store/counter/counter.selectors';


export const CounterPage = () => {
  const dispatch = useDispatch();
  const counter = useSelector(selectCounter )
  const itemsPerPallet = useSelector(selectItemsPerPallet)
  const material = useSelector(selectMaterial)
  const totalPallets = useSelector(selectTotalPallet)

  return <div>
    <h1>Counter: {counter}</h1>
    <h1>Total Pallets: {totalPallets} ({itemsPerPallet} items per Pallet / {material})</h1>

    <button onClick={() => dispatch(increment(10))}>+</button>
    <button onClick={() => dispatch(decrement(5))}>-</button>
    <button onClick={() => dispatch(setConfig({ material: 'plastic'}))}>plastic</button>
    <button onClick={() => dispatch(setConfig({ material: 'wood'}))}>wood</button>
    <button onClick={() => dispatch(setConfig({ itemsPerPallet: 10}))}>10 items</button>
    <button onClick={() => dispatch(setConfig({ itemsPerPallet: 5}))}>5 items</button>
  </div>
};

export interface CounterConfig {
  itemsPerPallet: number;
  material: string; // 'wood' | 'plastic'
}

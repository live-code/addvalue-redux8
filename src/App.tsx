import { AnyAction, combineReducers, configureStore, ThunkAction, ThunkDispatch } from '@reduxjs/toolkit';
import React from 'react';
import './App.css';
import { Root } from 'react-dom/client';
import { Provider, useDispatch } from 'react-redux';
import { Navbar } from './core/components/Navbar';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { httpStatusStore } from './core/store/http-status.store';
import { catalogReducers } from './features/catalog/store';
import { productsStore } from './features/catalog/store/products.store';
import { CounterPage } from './features/counter/CounterPage';
import { counterReducers } from './features/counter/store';
import { usersApi } from './features/users/store/users.api';
import { UsersPage } from './features/users/UsersPage';
import { CatalogPage } from './features/catalog/CatalogPage';
import { SettingsPage } from './features/settings/SettingsPage';

const rootReducer = combineReducers({
  todos: () => [],
  counter: counterReducers,
  catalog: catalogReducers,
  httpStatus: httpStatusStore.reducer,
  [usersApi.reducerPath]: usersApi.reducer
})

export type RootState = ReturnType<typeof rootReducer>;
export type AppThunk = ThunkAction<void, RootState, null, AnyAction>
export type AppDispatch = ThunkDispatch<RootState, null, AnyAction>
export const useAppDispatch = () => useDispatch<AppDispatch>()

export const store = configureStore({
  reducer: rootReducer,
  devTools: process.env.NODE_ENV !== "production",
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(usersApi.middleware)
})

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/counter" element={<CounterPage />} />
          <Route path="/users" element={<UsersPage />} />
          <Route path="/catalog" element={<CatalogPage />} />
          <Route path="/settings" element={<SettingsPage />} />
          <Route
            path="*"
            element={
              <Navigate to='/counter' />
            }
          />
        </Routes>
      </BrowserRouter>

    </Provider>
  );
}

export default App;
